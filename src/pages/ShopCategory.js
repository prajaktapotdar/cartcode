import React, { useContext, useState } from "react";
import './CSS/ShopCategory.css'
import {ShopContext} from "../context/ShopContext";
import Item from '../components/Item/Item';


const ShopCategory = (props) => {
  
    const {all_product} = useContext(ShopContext);
    return (
        <div className="shop-category">
            <img src={props.banner} alt="" />
        <div className="Shop-category-index">

        </div>
            <div className="shop-category-products  container mt-5 mb-5">
                {all_product.map((item, i)=>{
                 
                        return <Item key={i} id={item.id} name={item.name} image={item.image} new_price={item.new_price} old_price={item.old_price}/>
                   
               })}
             
            </div>
           
        </div>
    );
}
export default ShopCategory;