
import './App.css';
import Navbar from './components/Navbar/Navbar';
import Shop from './pages/Shop';
import Products from './pages/Products'
import { BrowserRouter, Routes, Route } from "react-router-dom";
import ShopCategory from './pages/ShopCategory';
import LoginSignin from './pages/LoginSignin';
import Cart from './pages/Cart';
import Footer from './components/Footer/Footer';
import men_banner from './components/Asset/banner_mens.png';
import women from './components/Asset/banner_women.png';
import kids_banner from './components/Asset/banner_kids.png';

function App() {
  return (
    <div>
      <BrowserRouter>
        <Navbar/>
        <Routes>
          <Route path='/' element={<Shop/>}/>
          <Route path='/Mens' element={<ShopCategory banner={men_banner} category="Men"/>}/>
          <Route path='/Womens' element={<ShopCategory banner={women} category="Women"/>}/>
          <Route path='/Kids' element={<ShopCategory banner={kids_banner} category="Kids"/>}/>
          <Route path='/Products' element={<Products/>}>
          <Route path=':productsId' element={<Products/>}/>
          </Route>
          <Route path='/Cart' element={<Cart/>}/>
          <Route path='/Login' element={<LoginSignin/>}/>
        </Routes>
        <Footer/>
      
      </BrowserRouter>
    </div>
  );
}

export default App;
