import React from "react";
import './Navbar.css';
import logo from '../Asset/logo.png';
import cart_icon from '../Asset/cart_icon.png';
import { useState } from "react";
import { Link } from "react-router-dom";


const Navbar = () => {

    const [menu, setMenu] = useState("Shop")

    return (

        <div className="Navbar">
            <div className="nav-logo">
                <img src={logo} alt="" />
                <p>SHOPPER</p>
            </div>
            <ul className="nav-menu">
                <li onClick={() => setMenu("Shop")}><Link to='/'>Shop</Link>  {menu === "Shop" ? <hr /> : <></>}</li>
                <li onClick={() => setMenu("Women")}><Link to='/Womens'>All Products</Link> {menu === "Women" ? <hr /> : <></>}</li>
                {/* <li onClick={() => setMenu("Men")}><Link to='/Mens'>Mens</Link>   {menu === "Men" ? <hr /> : <></>}</li> */}
                {/* <li onClick={() => setMenu("Kids")}><Link to='/Kids'>Kids</Link>   {menu === "Kids" ? <hr /> : <></>}</li> */}
            </ul >
            <div className="nav-lgin-cart">
                <Link to='/login'><button>Login</button></Link>
                <Link to='/cart'> <img src={cart_icon} alt="" /></Link>
                <div className="nav-cart-count">0</div>


            </div>
        </div>
    );
}

export default Navbar;

