import { Button } from "bootstrap";
import React from "react";
import exclusive_image from '../Asset/exclusive_image.png';
import './Offer.css'

const Offer = () => {
    return (
        <div className="offer">
            <div className="offer-left">
                <h1>Exclusive <br/> Offer For You </h1>
                <p>ONLY ON BEST SELLER PRODUCTS</p>
                <button>Check Now</button>
            </div>
            <div className="offer-right">
            <img src={exclusive_image} />
            </div>
        </div>
    )
}
export default Offer;