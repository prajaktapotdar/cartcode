import React from "react";
import './Footer.css'
import footer_logo from '../Asset/logo.png';
import instagram_icon from '../Asset/instagram_icon.png';
import pintester_icon from '../Asset/pintester_icon.png';
import whatsapp_icon from '../Asset/whatsapp_icon.png'
const Footer = () =>{
    return(
        <div className="footer">
            <div className="footer-logo ">
                <img src={footer_logo} />
                <p>SHOPPER</p>
            </div>
            <ul className="footer-menu">
                <li>Company</li>
                <li>Products</li>
                <li>About</li>
                <li>Contact</li>
            </ul>
            <div className="footer-social">
            <img src={instagram_icon} />
            <img src={pintester_icon} />
            <img src={whatsapp_icon} />
            </div>
            <hr/>
            <div className="copyright">
                <p>Copyright @2023 - All Right Reserved</p>
            </div>
        </div>
    )
}
export default Footer;