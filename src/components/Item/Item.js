import React, { useContext } from "react";
import './Item.css'

import { ShopContext } from "../../context/ShopContext";

 
const Item = (props) => {
      const {product} = props;
    const {addTocart} = useContext(ShopContext);
    return (
        <div className="item">
            <img src={props.image} alt="" />
            <p>{props.name}</p>
            <div className="item-price">
                <div className="item-price-new">
                    ${props.new_price}
                </div>
                <div className="item-price-old">
                    ${props.old_price}
                </div>
                <button onClick={()=> {addTocart(product.id)}} className="cart-button">Add to Cart</button>
            </div>
        </div>
    );
}
export default Item;
