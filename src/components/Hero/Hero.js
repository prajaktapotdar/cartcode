import React from "react";
import './Hero.css'
import hand_icon from '../Asset/hand_icon.png';
import arrow from '../Asset/arrow.png';
import hero_image from '../Asset/hero_image.png'

const Hero = () => {
    return (
        <div className=" hero">

            <div className="hero-left">
                <div>
                <h2>NEW ARRIVALS ONLY</h2>
                <div className="hero-hand-icon">
                    <p>new  <img src={hand_icon} alt="" width={60}/></p>

                    <p>collections</p>
                    <p>for everyone</p>
                </div>
                <div className="hero-latest-btn">
                    <div> Latest collections  <img src={arrow} alt="" /></div>
                   
                </div>
                </div>
            </div>
            <div className="hero-right">
                <img src={hero_image} alt=" " />

            </div>

        </div>
    );
}

export default Hero;