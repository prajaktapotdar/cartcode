import React, { useContext } from "react";
import './CartItems.css'
import { ShopContext } from "../../context/ShopContext";
import removeicon from '../Asset/cart_cross_icon.png';

const CartItems = () => {
const {all_product, cartItems,removeFromcart} = useContext(ShopContext);


  return (
    <div className="cartitems">
     <div className="cart-items-format">
        <p>products</p>
        <p>title</p>
        <p>price</p>
        <p>qauntity</p>
        <p>total</p>
        <p>remove</p>
     </div>
     <hr/>
     {all_product.map((e)=>{
     if(cartItems[e.id]>0)
     {
        return   <div>
        <div className="cart-format">
           <img src={e.image} className="carticon-product-icon"/>
           <p>{e.name}</p>
           <p>${e.new_price}</p>
           <button className="cartitems-quantity">{cartItems[e.id]}</button>
           <p>${e.new_price*cartItems[e.id] }</p>
           <img src={removeicon} onClick={()=>{removeFromcart(e.id)}}/>
           <hr/>
           </div> 
        </div>
     }
     }
     )
     }
     
    </div>
    
  )}
  
export default CartItems;