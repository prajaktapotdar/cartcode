import React from "react";
import './New-Collection.css';
import Item from "../Item/Item";
import new_collections from "../Asset/new_collections";

const NewCollection =() =>{
return(
    <div className="New-Collection">
        <h1>New Collection</h1>
        <hr/>
        <div className="collection">
            {new_collections.map((item ,i)=> {
                return <Item key={i} id={item.id} name={item.name} image={item.image} new_price={item.new_price} old_price={item.old_price} />

            }) }
        </div>
    </div>
)
}
export default NewCollection;